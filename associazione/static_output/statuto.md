Disposizioni generali
=====================

Denominazione
-------------

É costituita l’Associazione Culturale “HackLab Cormano”, d’ora in poi
denominata semplicemente Associazione.

L’Associazione non ha finalità di lucro e gli eventuali utili conseguiti
dovranno essere utilizzati per il conseguimento degli scopi
istituzionali.

Oggetto
-------

Scopo dell’Associazione è quello di promuovere la più ampia diffusione
dell’informatica e dell’elettronica come espressione della filosofia del
**Software Libero**[^1] e del **Hardware Libero**[^2], che garantiscono
a ciascuno la libertà di poter contribuire alla propria comunità
accedendo, creando, modificando, pubblicando e distribuendo le **Opere
Culturali Libere**[^3] collettivamente sviluppate.

L’Associazione riconosce inoltre nella **Cultura *Hacker* **[^4] un
costruttivo approccio di partecipazione alla nostra società e intende
dare il proprio contributo alla conoscenza e diffusione del significato
originale dei termini *hacker* [^5], *hacking* e *hacklab* [^6]: termini
che indicano la curiosità verso il funzionamento dei sistemi, la voglia
di condividerne la conoscenza e di partecipare al loro miglioramento.

L’Associazione intende perseguire i propri scopi tramite le seguenti
attività:

1.  organizzazione di un *hacklab* aperto a tutti i soci;

2.  pubblicazione e distribuzione di materiale informativo, formativo,
    critico, saggistico sotto forma di *Opere Culturali Libere*;

3.  utilizzo, studio, sviluppo, distribuzione e pubblicazione di
    *Software Libero*;

4.  utilizzo, studio, sviluppo e produzione di *Hardware Libero*;

5.  organizzazione di convegni e seminari;

6.  utilizzo dei mezzi di comunicazione radio e televisivi e delle reti
    informatiche in maniera funzionale ai propri scopi;

7.  collaborazione nei confronti di qualsiasi persona fisica, giuridica
    o realtà istituzionale che lo richiedesse, in conformità ai propri
    scopi.

Durata
------

L’Associazione ha durata illimitata e può essere sciolta con una
delibera dell’Assemblea dei soci in riunione straordinaria.

I Soci
======

Composizione dell’Associazione
------------------------------

Il numero dei soci è illimitato e può diventare socio chiunque si
riconosca nel presente statuto, indipendentemente dalla propria
appartenenza politica e religiosa, sesso, cittadinanza, appartenenza
etnica e professione. Agli aspiranti soci sono richiesti l’accettazione
dello statuto, il godimento di tutti i diritti civili e il rispetto
della civile convivenza.

Domanda di ammissione
---------------------

Chi intende aderire alla Associazione deve rivolgere espressa domanda
all’Assemblea dei soci recante la dichiarazione di condividere le
finalità associative e l’impegno ad accettarne e osservarne Statuto e
Regolamenti.

Soci
----

Sono soci tutti coloro che si riconoscono nei fini della Associazione,
che sono disposti a sostenerla economicamente per il raggiungimento
degli scopi istituzionali, che presentano domanda scritta di ammissione
alla Associazione. I soci si impegnano al pagamento della quota sociale
prevista e stabilita annualmente dall’Assemblea dei soci.

Diritti e doveri del socio
--------------------------

Tutti i soci hanno gli stessi doveri e godono degli stessi diritti nei
confronti dell’Associazione. I soci hanno diritto a:

1.  frequentare i locali a disposizione dell’Associazione;

2.  partecipare a tutte le iniziative e manifestazioni promosse
    dall’Associazione;

3.  riunirsi in assemblea per discutere e votare sulle questioni
    riguardanti l’Associazione;

4.  eleggere ed essere eletti membri degli organi dell’Associazione.

Hanno diritto di voto in Assemblea tutti i soci regolarmente iscritti.

Recesso del socio
-----------------

La qualifica di socio si perde per:

1.  mancato pagamento della quota sociale;

2.  espulsione o radiazione;

3.  dimissioni, che devono essere presentate per iscritto all’Assemblea
    dei soci.

4.  decesso;

Esclusione del socio
--------------------

L’Assemblea dei soci ha la facoltà di intraprendere azione disciplinare
nei confronti del socio, mediante - a seconda dei casi - il richiamo
scritto, la sospensione temporanea o l’espulsione o radiazione per i
seguenti motivi:

1.  mancato rispetto delle disposizioni dello statuto, di eventuali
    regolamenti o delle deliberazioni degli organi sociali;

2.  denigrazione dell’Associazione, dei suoi organi sociali, dei suoi
    soci;

3.  intralcio al buon andamento dell’Associazione, ostacolandone lo
    sviluppo e perseguendone lo scioglimento;

4.  commissione o provocazione gravi disordini durante le assemblee;

5.  appropriazione indebita dei fondi sociali, atti, documenti od altro
    di proprietà dell’Associazione;

6.  aver arrecato danni morali o materiali all’Associazione, ai locali
    ed alle attrezzature di sua pertinenza. In caso di dolo, il danno
    dovrà essere risarcito.

Ricorso
-------

Contro ogni provvedimento di sospensione, espulsione o radiazione, è
ammesso il ricorso entro 30 (trenta) giorni, sul quale decide in via
definitiva la prima assemblea utile dei soci.

Patrimonio sociale, entrate e rendiconto
========================================

Patrimonio sociale
------------------

Il patrimonio sociale dell’Associazione è indivisibile ed è costituito
da:

-   contributi, erogazioni e lasciti diversi;

-   fondo di riserva.

L’utilizzo del fondo di riserva è vincolato alla decisione
dell’Assemblea dei Soci.

Entrate
-------

Per l’adempimento dei suoi compiti la Associazione dispone delle
seguenti entrate:

1.  versamenti effettuati dai fondatori originari, dei versamenti
    ulteriori effettuati da detti fondatori e da quelli effettuati da
    tutti coloro che aderiscono all’Associazione;

2.  introiti realizzati nello svolgimento della propria attività.

L’Assemblea dei soci stabilisce annualmente la quota di iscrizione
all’Associazione.

I versamenti al fondo di dotazione possono essere di qualsiasi entità,
fatti salvi i versamenti minimi come sopra determinati, per l’iscrizione
annuale.

Qualsiasi tipo di versamento non crea altri diritti di partecipazione o
quote indivise di partecipazione trasmissibili a terzi.

Tutti i versamenti effettuati all’Associazione, comprese le quote di
iscrizione, sono a fondo perduto e non sono rivalutabili in nessun caso,
nemmeno in caso di scioglimento o estinzione dell’Associazione, morte
del socio, recesso o esclusione del socio dall’Associazione.

Rendiconto
----------

Il rendiconto comprende l’esercizio sociale dal 1 gennaio al 31 dicembre
di ogni anno e deve essere presentato all’assemblea dei soci entro il 30
aprile successivo. Ulteriore deroga può essere prevista in caso di
comprovata necessità o impedimento.

Il rendiconto dovrà essere composto da un documento che illustri e
riassuma la situazione finanziaria dell’Associazione, con particolare
riferimento allo stato del fondo di riserva.

L’Assemblea
===========

Composizione
------------

L’Assemblea, Ordinaria e Straordinaria, è l’organo deliberativo
dell’Associazione; hanno diritto a parteciparvi tutti i soci
regolarmente iscritti.

Competenze dell’Assemblea Ordinaria
-----------------------------------

L’Assemblea Ordinaria ha le seguenti competenze:

1.  approvare il rendiconto economico e finanziario;

2.  approvare le linee generali del programma di attività ed il relativo
    documento economico di previsione;

3.  eleggere gli organi dell’Associazione alla fine del mandato o in
    seguito alle dimissioni degli stessi, votando la preferenza a
    nominativi scelti tra i soci;

4.  deliberare su tutte le questioni attinenti la gestione sociale.

Convocazione dell’Assemblea
---------------------------

L’Assemblea **ordinaria** viene convocata una volta all’anno nel periodo
che va dal 1 gennaio al 30 aprile.

L’Assemblea **straordinaria** viene convocata tutte le volte che
l’Assemblea dei soci lo reputi necessario o ogni qual volta ne faccia
richiesta almeno un quinto dei soci.

L’Assemblea dovrà aver luogo entro 30 (trenta) giorni dalla data in cui
viene richiesta; la convocazione avviene mediante semplice comunicazione
scritta o per posta elettronica indirizzata ai singoli Soci.

L’avviso di convocazione è spedito almeno 21 (ventuno) giorni prima
dell’Assemblea, e indica il luogo, la data e l’ora in cui si terrà
l’Assemblea stessa, con il relativo ordine del giorno.

Costituzione dell’Assemblea
---------------------------

L’Assemblea, sia ordinaria che straordinaria, è regolarmente costituita
alla presenza di un terzo dei soci.

Deleghe
-------

Ciascun socio può essere rappresentato in assemblea tramite delega
scritta, ciascun delegato può raccogliere fino a 5 deleghe.

Delibere Assembleari
--------------------

L’Assemblea, sia ordinaria che straordinaria, delibera a maggioranza
semplice (la metà più uno) sull’insieme dei soci presenti. Le votazioni
in Assemblea ordinaria e straordinaria avvengono per alzata di mano o
per appello nominale.

Eccezioni alle delibere
-----------------------

Per delibere riguardanti modifiche allo Statuto o ai Regolamenti è
indispensabile la presenza della maggioranza assoluta dei soci ed il
voto favorevole di almeno tre quinti dei partecipanti.

Verbalizzazione
---------------

L’Assemblea, all’inizio di ogni sessione, elegge tra i soci presenti un
presidente e un segretario di assemblea: il segretario provvede a
redigere i verbali delle deliberazioni, verbali devono essere
sottoscritti dal presidente dell’Assemblea e dal segretario.

I verbali dell’Assemblea sono messi a disposizione dei soci.

Gli organi sociali
==================

Organi dell’Associazione
------------------------

Gli organi dell’Associazione sono:

-   il Presidente: ha la rappresentanza legale dell’Associazione ed è il
    responsabile di ogni attività della stessa;

-   il Segretario: cura gli aspetti amministrativi dell’Associazione.

L’Associazione può inoltre distribuire fra i suoi componenti altre
funzioni attinenti a specifiche esigenze legate alle proprie attività.

Compiti del Presidente
----------------------

Compiti del Presidente sono:

-   dare seguito alle delibere dell’assemblea;

-   formulare i programmi di attività sociale sulla base delle linee
    approvate dall’Assemblea e del relativo documento economico di
    previsione;

-   stipulare tutti gli atti e i contratti inerenti le attività sociali;

-   decidere le modalità di partecipazione dell’Associazione alle
    attività organizzate da altre Associazioni ed Enti, e viceversa, se
    compatibili con i principi del presente Statuto.

Compiti del Segretario
----------------------

Compiti del Segretario sono:

-   predisporre il rendiconto economico e finanziario consuntivo;

-   predisporre il documento economico di previsione.

Collaborazioni esterne
----------------------

L’Assemblea dei soci può avvalersi, per compiti operativi o di
consulenza, di commissioni di lavoro da esso nominate, nonché
dell’attività volontaria di cittadini non soci, in grado, per competenze
specifiche, di contribuire alla realizzazione di specifici programmi.

Scioglimento dell’Associazione
==============================

La decisione motivata di scioglimento dell’Associazione deve essere
presa da almeno i quattro quinti dei soci aventi diritto al voto, in
un’Assemblea valida alla presenza della maggioranza assoluta dei
medesimi.

L’Assemblea decide sulla devoluzione del patrimonio residuo, dedotte le
eventuali passività, per uno o più scopi stabiliti dal presente Statuto
e comunque per associazioni con finalità analoghe o ai fini di pubblica
utilità, procedendo alla nomina di uno o più liquidatori scelti
preferibilmente tra i soci.

Disposizioni finali
===================

Per quanto non previsto dallo Statuto o dal Regolamento interno, decide
l’Assemblea ai sensi del Codice Civile e delle leggi vigenti.

[^1]: <http://www.gnu.org/philosophy/free-sw.it.html>

[^2]: <http://www.oshwa.org/definition/Italian>

[^3]: <http://freedomdefined.org/Definition/It>

[^4]: <http://en.wikipedia.org/wiki/Hacker_culture>

[^5]: <http://www.cs.berkeley.edu/~bh/hacker.html>

[^6]: <http://en.wikipedia.org/wiki/Hackerspace>

