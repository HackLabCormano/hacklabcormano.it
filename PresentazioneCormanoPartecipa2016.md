Un hacklab a Cormano? E cos'è un hacklab? Hacklab (*hacking* + *laboratory*) è un termine anglosassone che indica un luogo, sia fisico che virtuale (su web) dove appassionati di tecnologia si trovano, sperimentano, si scambiano conoscenze, coltivano una passione per l'esplorazione di meccanismi e sistemi e, più in generale, si divertono!

L'origine è lontana: negli anni '60 del secolo scorso i primi *hacker* (un termine piuttosto bistrattato, **non** ha il significato negativo che i media gli attribuiscono) si radunavano per smontare e rimontare - bada bene: senza perdere vitine nel processo - attrezzature, meccanismi, elettrodomestici, trenini elettrici, serrature e... i primi computer.

Negli anni '90 nascono i primi *user group* dedicati al mondo del software (GNU/Linux, altri sistemi operativi, linguaggi di programmazione) e dell'hardware (dagli home computer degli anni '80 fino al *modding* dei personal computer).

Nel primo decennio del nuovo secolo, con la nascita di tecnologie come Arduino (un microscopico computer facilmente collegabile a sensori e attuatori), stampa 3D, hardware libero e con l'abbassamento notevole dei costi di produzione si assiste ad una esplosione dell'interesse per le micro-piattaforme (Arduino, RaspberryPI, Beaglebone, etc.) e per il cosiddetto "mondo maker", cioè quel contesto in cui i singoli possono autocostruirsi a basso costo oggetti tecnologici anche molto complessi e molto funzionali, di solito applicati al mondo della domotica (informatica per la gestione delle abitazioni) o al cosiddetto *wearable computing* (tecnologia indossabile).

Perché proprio a Cormano? L'idea dell'hacklab nasce da un docente, Andrea Trentini, dell'Università degli Studi di Milano, recentemente diventato cittadino cormanese. Trentini ha insegnato per anni nei corsi di Sistemi Operativi e di Programmazione, ha istituito con la collega Fiorella De Cindio il corso di "Cittadinanza Digitale e Tecnocivismo" e dal 2011 organizza un *camp* per gli studenti del Dipartimento di Informatica di Milano che, su base volontaria, si trovano periodicamente per sperimentare con Arduino e tecnologie analoghe... insomma un hacklab all'interno dell'università. Arrivato a Cormano, Trentini ha pensato di riportare l'esperienza univeristaria in un contesto civico e ha quindi contattato l'amministrazione comunale per organizzare inizialmente una serata, poi un corso gratuito e infine, raggiunto un certo numero di accoliti, ha proposto una iniziativa stabile e regolare - la fondazione della associazione senza scopo di lucro "HackLab Cormano"  - chiedendo l'avallo del Comune. Avallo che è stato dato con entusiasmo: la Biblioteca Volontè fornisce infatti i locali una sera alla settimana per il ritrovo *de visu* degli iscritti.

Nei primi mesi del 2016, chi aveva partecipato alle iniziative organizzate ha collaborato alla redazione del manifesto (https://gitlab.com/HackLabCormano/hacklabcormano.it/blob/master/associazione/manifesto.md) e dello statuto della associazione, che si è poi costituita il 27 Aprile 2016.

A inizio Giugno 2016 l'associazione ha iniziato i lavori, gli iscritti si trovano il mercoledì sera in biblioteca dalle 21 alle 23 circa. Al momento il tema caldo è "Arduino" e quindi le serate sono dedicate allo studio delle basi dell'architettura di Arduino e del suo linguaggio di programmazione *Wiring*. In futuro chissà... si pensa già a temi come le reti, GNU/Linux, altre micropiattaforme (RaspberryPI, Beaglebone, CHIP, etc.), linguaggi di programmazione, etc. Ogni serata è composta da momenti di spiegazione "tradizionale" (frontale) e da momenti in cui ogni partecipante si dedica alla sperimentazione personale di ciò che ha visto "alla lavagna" (che non c'è, usiamo un sistema di video sharing).

Come partecipare? Il 21 Settembre 2016 (data da confermare) HackLab Cormano organizzerà una serata aperta a tutti in cui si potrà vedere e toccare con mano (con attenzione!) i progetti dell'associazione.

=== Contatti ===

Presidente: Andrea Trentini
Segretario: Giovanni Biscuolo

SITO: http://hacklabcormano.it

EMAIL: associazione@hacklabcormano.it
PEC: associazione@pec.hacklabcormano.it

Serate: i mercoledì 21:00-23:00

Dove: Biblioteca Volontè Via Edison, 8, Cormano (http://webopac.csbno.net/library/cormano)
