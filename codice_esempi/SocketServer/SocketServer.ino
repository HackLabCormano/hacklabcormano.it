/* serve:
 * wifi
 * stringhe
 * http?
 *
 * https://github.com/esp8266/Arduino#documentation
 *
 * nmcli c up ESP8266;ifconfig
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
//#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

//////////////////////////////////////////////////
#include "DHT.h"
#define DHTTYPE DHT11   // DHT 11
#define DHTPIN D4
DHT dht(DHTPIN, DHTTYPE);

#define REFRESH 10
#define SSID "ESP8266"
#define PASS "arduino2linux"
#define COMMAND "CMD:"
#define CMDLEN 4
#define LED D8
#define LAMPS 10

float humidity=0,temperature=0;

boolean led=LOW;

//IPAddress local_IP(192,168,4,1);
//IPAddress gateway(192,168,4,1);
//IPAddress subnet(255,255,255,0);

WiFiServer server(80);
//String request="";


void readSensor() {
    /////////////////////////////////////
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    humidity = dht.readHumidity();
    // Read temperature as Celsius (the default)
    temperature = dht.readTemperature();
}

void lamps() {
    for(int i=0; i<LAMPS; i++) {
        digitalWrite(LED,HIGH);
        delay(20);
        digitalWrite(LED,LOW);
        delay(20);
    }
}

void command(String cmd) {
    if(cmd=="ACCENDI") {
        Serial.println("accendo");
        led=HIGH;
    }
    if(cmd=="SPEGNI") {
        Serial.println("spengo");
        led=LOW;
    }
    if(cmd=="LAMPEGGIA") {
        Serial.println("lampeggio");
        lamps();
    }
    digitalWrite(LED,led);
}


void containsCommand(String req) {

    /*
    Serial.println("*** req:");
    Serial.print(req);
    Serial.println("***");
    */

    int cmd=req.indexOf(COMMAND);
    if(cmd >=0) {
        req=req.substring(cmd); // inizio comando
        req=req.substring(CMDLEN,req.indexOf("/")); // fine comando

        if(req.length()>0) {
            Serial.print("COMANDO:");
            Serial.println(req);
            command(req);
        }
    }
}

void setupWifiAP() {
    //Serial.print("Setting soft-AP configuration ... ");
    //Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");

    Serial.print("Setting soft-AP ... ");
    //Serial.println(WiFi.softAP(SSID,PASS) ? "Ready" : "Failed!");
    Serial.println(WiFi.softAP(SSID) ? "Ready" : "Failed!");

    Serial.print("Soft-AP IP address = ");
    Serial.println(WiFi.softAPIP());
}

void httpResponse(WiFiClient client) {
    // send a standard http response header
    client.println("HTTP/1.1 200 OK\nContent-Type: text/html\nConnection: close\nCache-control: no-cache\nRefresh: ");
    client.println(REFRESH);

    client.println("\n\n<!DOCTYPE HTML><html><body>");

    // output temp and humid
    client.print("<hr>Temp: ");
    client.println(temperature);
    client.print("<hr>Humid: ");
    client.println(humidity);
    client.print("<hr>LED: ");
    client.println(led);
    client.println("<hr><a href=\"/CMD:ACCENDI/\">ACCENDI LED</a><hr><a href=\"/CMD:SPEGNI/\">SPEGNI LED</a><hr><a href=\"/CMD:LAMPEGGIA/\">LAMPEGGIA LED</a><hr></body></html>\n");

}

void waitForClient() {
    WiFiClient client = server.available();

    if (client) {
        Serial.print("connected: ");
        Serial.println(WiFi.softAPgetStationNum());

        String request="";
        while (client.available()) {
            request.concat((char)client.read());
        }

        // gestire request
        //Serial.print(request);
        containsCommand(request);
        httpResponse(client);
        delay(50);
        // chiudere
        client.stop();
    }
}


/////////////////////////////////////////////////////

void setup()
{
    Serial.begin(115200);
    Serial.println("=== Setup...");

    pinMode(LED,OUTPUT);

    setupWifiAP();
    delay(500);

    server.begin();
    delay(500);
}

void loop()
{
    readSensor();

    if(millis()%10000<50)
    {
        //Serial.println("=== Loop...");
        Serial.print("=== Connected: ");
        Serial.println(WiFi.softAPgetStationNum());
        Serial.print("=== Mem: ");
        Serial.println(ESP.getFreeHeap());
        Serial.print("=== Sensor: ");
        Serial.print(temperature);
        Serial.print(",");
        Serial.println(humidity);
    }

    waitForClient();

    delay(50);
}
